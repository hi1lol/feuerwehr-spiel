/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feuerwehr.editor;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import feuerwehr.main.Material;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcel
 */
public class MaterialReader {
    static String startDir = System.getProperty("user.dir");
    static File materialfolder = new File(startDir+File.separator+"materials");
    //public MaterialReader(){ }
    public static List<Material> getMaterialList(){
        System.out.println(materialfolder);
        List<Material> list = new ArrayList<>();
        for(File file : materialfolder.listFiles()){
            if(file.getName().contains(".Json")){
            
            
                System.out.println(file.getAbsolutePath());
                
                try{
                    FileReader fr = new FileReader(file);
                    BufferedReader br = new BufferedReader(fr);
                    StringBuilder sb = new StringBuilder();
                    String line = br.readLine();

                    while (line != null) {
                        sb.append(line);
                        sb.append(System.lineSeparator());
                        line = br.readLine();
                    }
                    Gson gson = new Gson();
                    Material mat = gson.fromJson(sb.toString(), Material.class);
                    list.add(mat);
                }catch(JsonSyntaxException | IOException e){
                    e.printStackTrace();
                }
            }
        }
        return list;
    }
}
