/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feuerwehr.editor;


import com.google.gson.Gson;
import feuerwehr.main.Card;
import static feuerwehr.main.Card.MA;
import feuerwehr.main.Material;
import feuerwehr.main.Start;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Marcel
 */
public class editorFrame {
    
    //public editorCard[] elemente;
    public static List<Card> elemente = new  ArrayList<>();
    
    JFrame frame = new JFrame("Editor");
    JPanel mainpanel = new JPanel();
    JPanel editorpanel = new JPanel();
    JScrollPane editorscrollpane = new JScrollPane(editorpanel);
    JPanel toolspanel = new JPanel();
    JPanel propspanel = new JPanel();
    JButton saveBtn = new JButton();
    JButton backBtn = new JButton();
    Date date = new Date();
    SimpleDateFormat datef = new SimpleDateFormat("dd.MM.YYYY-HH.mm");
    JFileChooser fileChooser = new JFileChooser();
    JSplitPane splitpaneltools = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    static List<Material> matlist = MaterialReader.getMaterialList();
    //JLis
    public void init(int x, int y, List<Card> cardlist,Start mainWindow){
        frame.setSize(700, 400);
        frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        //Editor Layout
        GridLayout fieldlayout = new GridLayout(x,y,2,2);
        editorpanel.setLayout(fieldlayout);
        //SaveButton
        backBtn.setText("Zurück!");
        saveBtn.setText("Speichern");
        
        
        if(cardlist!=null){
            for(Card card : cardlist){
                
                card.addMouseListener(MA(card));
                elemente.add(card);
                editorpanel.add(card);
            }
        }else{
            for(int i = x*y; i>0; i-- ){
                Card label = new Card();
                label.addMouseListener(MA(label));
                elemente.add(label);
                editorpanel.add(label);
            }
        }
        backBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainWindow.setVisible(true);
                frame.dispose();
            }});
        
        saveBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sdate = datef.format(date);
                //System.out.println(elemente);
                fileChooser.setSelectedFile(new File(sdate+".level"));
                fieldSaveType saveobj = new fieldSaveType(x, y, elemente);
                File file = null;
                if (fileChooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
                    file = fileChooser.getSelectedFile();
                    
                    
                    try{ 
                        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));

                        oos.writeObject(saveobj);
                        oos.close();
                        System.out.println("Saved!");

                    } catch (Exception ex) {
			ex.printStackTrace();
                    }
                    
                    
                }
                
                
                
            }
        });
        toolspanel.setLayout(new FlowLayout());
        toolspanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        toolspanel.add(backBtn);
        toolspanel.add(saveBtn);
        
        
        splitpaneltools.setTopComponent(toolspanel);
        splitpaneltools.setBottomComponent(editorscrollpane);
        
        frame.add(splitpaneltools);
        frame.setVisible(true);
        
    }
    //HSplit
    
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
    
    
}
