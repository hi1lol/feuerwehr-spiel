/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feuerwehr.editor;

import com.google.gson.annotations.Expose;
import feuerwehr.main.Card;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Marcel
 */
public class fieldSaveType implements Serializable {
    @Expose
    private int width;
    @Expose
    private int height;
    //private editorCard card[];
    @Expose
    private List<Card> card;
    
    public fieldSaveType(int width, int height, List<Card> card){
        System.out.println(card.get(0).getMaterial(0));
        this.width = width;
        this.height = height;
        this.card = card;
    }
    
    public int getWidth(){
        return this.width;
    }
    
    public int getHeight(){
        return this.height;
    }
    
    public List<Card> getCardList(){
        return this.card;
    }
    
    
}
