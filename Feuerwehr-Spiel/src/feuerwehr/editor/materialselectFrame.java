/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feuerwehr.editor;

import feuerwehr.main.Card;
import feuerwehr.main.Material;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javafx.scene.control.SplitPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToggleButton;

/**
 *
 * @author Marcel
 */
public class materialselectFrame {
    Card card;
    JDialog frame = new JDialog();
    JPanel propspanel = new JPanel();
    JPanel updownpanel = new JPanel();
    JButton saveBtn = new JButton("Speichern");
    JButton addBtn = new JButton("Hinzufügen");
    JButton deleteBtn = new JButton("Löschen");
    JButton upBtn = new JButton("Hoch");
    JButton downBtn = new JButton("Runter");
    
    DefaultListModel<Material> dlm = new DefaultListModel<>();
    
    JComboBox<String> comboboxcolor;
    JCheckBox hiddenchk = new JCheckBox("Versteckt");
    //GridLayout fieldlayout = new GridLayout(1,3,2,2);
    String[] heat = { "Kalt", "Warm", "Rauch", "Neutral" };
    JSplitPane splitpanel3 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    JSplitPane splitpanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    JSplitPane splitpanel2 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    JSplitPane splitpanel4 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    JList jlist;
    JList selectedlist;
    JScrollPane listscrollpane;
    JScrollPane selectedlistscrollpane;
    public void init(Card card){
        this.card = card;
        
        frame.setPreferredSize(new Dimension(400, 300));
        frame.setMinimumSize(new Dimension(400, 300));
        frame.setTitle("Material Auswahl");
        hiddenchk.setSelected(card.isHidden());
        //propspanel.setLayout(fieldlayout);
        //levelpanel.add(layer1);
        jlist = new JList(editorFrame.matlist.toArray());
        jlist.setSelectedIndex(0);
        
        for(Material mat : card.getMaterialList()){
            dlm.addElement(mat);
        }
        
        selectedlist = new JList(dlm);
        selectedlist.setSelectedIndex(0);
        
        saveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtnActionPerformed(evt);
            }
        });
        addBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBtnActionPerformed(evt);
            }
        });
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });
        upBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                upBtnActionPerformed(evt);
            }
        });
        downBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downBtnActionPerformed(evt);
            }
        });
        propspanel.add(addBtn);
        propspanel.add(deleteBtn);
        
        GridLayout updownlayout = new GridLayout(2,1,2,2);
        updownpanel.setLayout(updownlayout);
        updownpanel.add(upBtn);
        updownpanel.add(downBtn);
        
        
        listscrollpane = new JScrollPane(jlist);
        selectedlistscrollpane = new JScrollPane(selectedlist);
        comboboxcolor = new JComboBox<>(heat);
        
        propspanel.add(comboboxcolor);
        propspanel.add(hiddenchk);
        
        splitpanel4.setLeftComponent(selectedlistscrollpane);
        splitpanel4.setRightComponent(updownpanel);
        splitpanel3.setBottomComponent(propspanel);
        splitpanel3.setTopComponent(listscrollpane);
        splitpanel2.setRightComponent(splitpanel3);
        splitpanel2.setLeftComponent(splitpanel4);
        splitpanel.setTopComponent(splitpanel2);
        splitpanel.setBottomComponent(saveBtn);
        frame.add(splitpanel);
        load();
        frame.setVisible(true);
    }
    
    
    private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {
        switch((String) comboboxcolor.getSelectedItem()){
            case "Warm":
                card.setColor(Color.RED);
                break;
            case "Kalt":
                card.setColor(Color.BLUE);
                break;
            case "Rauch":
                card.setColor(Color.BLACK);
                break;
            case "Neutral":
                card.setColor(Color.WHITE);
                break;
        }
        card.setHidden(hiddenchk.isSelected());
        if(card.getMaterialListSize()>0)
            card.setActiveMaterial(0);
        
        card.revalidate();
    }
    
    
    private void addBtnActionPerformed(java.awt.event.ActionEvent evt) { 
        
        for(Object obj : jlist.getSelectedValuesList()){
            card.addMaterial((Material) obj);
            dlm.addElement((Material) obj);
            System.out.println(card.getMaterialList().toString());
        }
        //card.setActiveMaterial(0);
    }
    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {
        if(selectedlist.getSelectedIndex()>=0){
            for(Object obj : selectedlist.getSelectedValuesList()){
            dlm.removeElement((Material) obj);
            card.removeMaterial((Material) obj);
            }
        }
    }
    private void upBtnActionPerformed(java.awt.event.ActionEvent evt) {
        Material selectedItem = (Material) selectedlist.getSelectedValue();//get item value
        int itemIndex = selectedlist.getSelectedIndex();// get item index
        DefaultListModel model = (DefaultListModel)selectedlist.getModel();// get list model
        
        if(itemIndex > 0){
            
            model.remove(itemIndex);// remove selected item from the list
            card.list.remove(itemIndex);
            model.add(itemIndex - 1, selectedItem);// add the item to a new position in the list
            card.list.add(itemIndex - 1, selectedItem);
            selectedlist.setSelectedIndex(itemIndex - 1);// set selection to the new item
        } 
    }
    private void downBtnActionPerformed(java.awt.event.ActionEvent evt) { 
        Material selectedItem = (Material) selectedlist.getSelectedValue();
        int itemIndex = selectedlist.getSelectedIndex();
        DefaultListModel model = (DefaultListModel)selectedlist.getModel();
        
        if( itemIndex < model.getSize() -1 ){
            model.remove(itemIndex);
            card.list.remove(itemIndex);
            model.add(itemIndex + 1, selectedItem);
            model.add(itemIndex + 1, selectedItem);
            selectedlist.setSelectedIndex(itemIndex + 1);
        }
    }
    
    private void load(){
    
        if(card.getColor()==Color.BLUE){
            comboboxcolor.setSelectedItem("Kalt");
        }else if(card.getColor()==Color.RED){
            comboboxcolor.setSelectedItem("Warm");
        }else if(card.getColor()==Color.BLACK){
            comboboxcolor.setSelectedItem("Rauch");
        }else{
            comboboxcolor.setSelectedItem("Neutral");
        }
        
    }
}
