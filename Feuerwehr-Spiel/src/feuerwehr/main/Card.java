/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feuerwehr.main;


import feuerwehr.editor.editorFrame;
import feuerwehr.editor.materialselectFrame;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Marcel
 */
public class Card extends JLabel {
    
    protected boolean hidden = true;
    protected Color color = Color.WHITE;
    protected int activeMaterial=0;
    protected int sim =0;
    public List<Material> list = new ArrayList<>();
    //private String filepath = "";
    //static String startDir = System.getProperty("user.dir");
    protected String picpath = System.getProperty("user.dir")+File.separator+"images"+File.separator;
    protected String standardpic = System.getProperty("user.dir")+ File.separator+"default.jpg";
    
    public Card(){
        //list.clear();
        //list.add(new Material("default", startDir));
        //setActiveMaterial(0);
       
        this.setPreferredSize(new Dimension(128, 128));
        this.setMaximumSize(new Dimension(128, 128));
        this.setOpaque(true);
        this.setBackground(color);
    }
    
    public void init(){
    
    }
    
    public int getMaterialListSize(){
        return list.size();
    }
    
    public void setActiveMaterial(int i){
        if(i<0||i>=list.size())
            return;
        ImageIcon img = new ImageIcon(picpath+list.get(i).getPicPath());
        this.setIcon(img);
        this.setHorizontalAlignment(CENTER);
    }
    
    
    public static MouseAdapter MA(Card card){
       return new MouseAdapter(){  
            public void mouseClicked(MouseEvent e){  
             java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new materialselectFrame().init(card);
            }
        });
            }
        };
    }
   
    
    
    
    public boolean isHidden(){
        return this.hidden;
    }
    public void setHidden(boolean b){
        this.hidden = b;
    }
    public Color getColor(){
        return this.color;
    }
    public void setColor(Color color){
        this.color = color;
        this.setBackground(this.color);
    }
    public int getActiveMaterialID(){
        return activeMaterial;
    }
    public Material getMaterial(int i){
        return this.list.get(i);
    }
    public List<Material> getMaterialList(){
        return list;
    }
    public void addMaterial(Material mat){
        list.add(mat);
    }
    public void removeMaterial(Material mat){
        list.remove(mat);
    }
    public void setMaterialList(List<Material> list){
        this.list = list;
    }

    
    
}
