/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feuerwehr.main;

import java.io.Serializable;

/**
 *
 * @author Marcel
 */
public class Material implements Serializable{
    private String name;
    private String picpath;
    
    
    public Material(String name,String picpath){
    
        this.name = name;
        this.picpath = picpath;
    }
    
    @Override
    public String toString(){
        return name;
    }
    public String getPicPath(){
        return picpath;
    }
}
