/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feuerwehr.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.ImageIcon;
import static javax.swing.SwingConstants.CENTER;

/**
 *
 * @author Marcel
 */
public class simCard extends Card {
    
     public simCard(boolean hidden, Color color,int activeMaterial, List<Material> list ){
         this.hidden = hidden;
         this.color=color;
         this.activeMaterial=activeMaterial;
         this.list=list;
        if(this.hidden){
            ImageIcon img = new ImageIcon(standardpic);
            this.setIcon(img);
            this.setHorizontalAlignment(CENTER);
            this.setColor(color.WHITE);
        }else{
            setActiveMaterial(0);
            this.setBackground(color);
        }
        this.setPreferredSize(new Dimension(128, 128));
        this.setMaximumSize(new Dimension(128, 128));
        this.setOpaque(true);
        this.removeMouseListener(Card.MA(this));
        this.addMouseListener(MA(this));
    }
     
        private MouseAdapter MA(simCard card){
        return new MouseAdapter(){  
           
            public void mouseClicked(MouseEvent e){  
                if(card.hidden){
                    card.hidden = false;
                    card.setBackground(color);
                    setActiveMaterial(0);
                }else{
                    if(activeMaterial<list.size()){
                        activeMaterial++;
                        if(activeMaterial<list.size()){
                            setActiveMaterial(activeMaterial);
                        }else{
                            card.setIcon(null);
                        }
                    }
                }
                
            }
        };
    }
}
