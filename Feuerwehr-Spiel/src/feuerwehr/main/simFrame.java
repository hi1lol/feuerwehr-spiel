/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feuerwehr.main;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

/**
 *
 * @author Marcel
 */
public class simFrame {
    List<simCard> list = new ArrayList<>();
    JFrame frame = new JFrame("Sim");
    JSplitPane splitpanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    JPanel fieldpanel = new JPanel();
    JScrollPane scrollpanel = new JScrollPane(fieldpanel);
    JPanel clock = new JPanel();
    JButton backBtn = new JButton("Zurück");
    time time = new time();
    
    public void init(int x, int y, List<simCard> cardlist,Start mainWindow){
        
        frame.setSize(700, 400);
        frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        //Editor Layout
        GridLayout fieldlayout = new GridLayout(x,y,2,2);
        clock.setLayout(new BoxLayout(clock, BoxLayout.Y_AXIS));
        fieldpanel.setLayout(fieldlayout);
        
        backBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainWindow.setVisible(true);
                frame.dispose();
            }
        });
        
        
        for(int i = 0; i<x*y; i++ ){
                
                
         System.out.println("TESTE!");
            
                JLabel label = cardlist.get(i);
                //System.out.println(cardlist.get(i).getMaterialList().get(0).toString());
                list.add(cardlist.get(i));
                fieldpanel.add(label);
        }
        //splitpanel.setDividerLocation(-225);
        time.setMinimumSize(new Dimension(225, 214));
        time.setMaximumSize(new Dimension(225, 214));
        clock.add(time);
        clock.add(backBtn);
        splitpanel.setLeftComponent(scrollpanel);
        splitpanel.setRightComponent(clock);
        frame.add(splitpanel);
        frame.setVisible(true);
        
    }
    
}
