package feuerwehr.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.Calendar;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class time extends JPanel implements Runnable, KeyListener {

    static JPanel panel = new JPanel();
    JButton stop;
    Thread timer = null;
    int startTime;
    int remainingTime;
    int elapsedTime;
     int a=0;
    Font font = new Font("SansSerif", Font.BOLD, 24);
    Image manometerImage;

    public time() {
        this.setPreferredSize(new Dimension(225, 214));
        manometerImage = this.getToolkit().getImage(System.getProperty("user.dir")+File.separator+"images"+File.separator+"manometer.jpg");
        Calendar calendar = Calendar.getInstance();
        startTime = calendar.get(Calendar.SECOND) + calendar.get(Calendar.MINUTE) * 60 + calendar.get(Calendar.HOUR) * 3600;
        timer = new Thread(this);
        timer.start();
        panel.addKeyListener(this);
    }

    public void paint(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, 128, 128);
        g.setColor(Color.BLACK);
        g.setFont(font);
        String minuteStr = "" + (remainingTime / 60);
        if (minuteStr.length() == 1) {
            minuteStr = "0" + minuteStr;
        }
        String secondStr = "" + (remainingTime % 60);
        if (secondStr.length() == 1) {
            minuteStr = "0" + minuteStr;
        }
        g.drawString(minuteStr + ":" + secondStr, 8, 32);
        g.setColor(Color.RED);
        g.fillArc(50, 50, 64, 64, 2, -remainingTime / 6);
        g.setColor(Color.BLACK);
        g.drawOval(50, 50, 64, 64);

        g.drawImage(manometerImage, 0, 0, this);
        int angle = (int) (225 - remainingTime / 6.6667);
        g.setColor(Color.RED);
        g.drawLine(112, 107, 112 + (int) (Math.cos(Math.toRadians(angle)) * 90), 107 - (int) (Math.sin(Math.toRadians(angle)) * 90));
    }

    @Override
    public void run() {
        while (timer != null) {
            //HIER BUON STOP START


            Calendar calendar = Calendar.getInstance();
            int actualTime = calendar.get(Calendar.SECOND) + calendar.get(Calendar.MINUTE) * 60 + calendar.get(Calendar.HOUR) * 3600;
            elapsedTime = actualTime - startTime;
            remainingTime = 1800 - elapsedTime;
            //System.out.println(elapsedTime + "s");
            repaint();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            if (timer != null) {
                // Beim Stoppen
                a = remainingTime;

                timer = null;
            } else {
                //Beim startten
                remainingTime = a;
                timer = new Thread(this);
                timer.start();
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
